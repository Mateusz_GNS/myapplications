package com.mateuszg17.calculatorgns.controllers;

import com.mateuszg17.calculatorgns.consoleVer.ReversePolishNotationLogic;
import com.mateuszg17.calculatorgns.model.InputLine;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@AllArgsConstructor
@Controller
public class CalculatorController {

    private final com.mateuszg17.calculatorgns.services.CalculatorService calculatorService;

    @GetMapping("/")
    public String showCalculator(Model model) {

        model.addAttribute("inputLine", new InputLine());
        return "calculator";
    }

    @PostMapping("/calculate")
    public String doCalculation(@ModelAttribute("inputLine") InputLine inputLine, Model model) {

        InputLine validatedAndComputed = calculatorService.validateAndCalculate(inputLine);

        model.addAttribute("result", validatedAndComputed.getExpressionLine());
        model.addAttribute("alert", validatedAndComputed.getAlert());

        return "calculator";
    }

}

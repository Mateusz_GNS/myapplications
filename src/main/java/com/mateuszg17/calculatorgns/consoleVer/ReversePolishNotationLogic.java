package com.mateuszg17.calculatorgns.consoleVer;

import org.springframework.stereotype.Controller;

import java.util.Stack;
import java.util.StringTokenizer;

@Controller
public class ReversePolishNotationLogic {

    private static String notDigits = "+-*/()";


    private int priorityCheck(String operator) {
        if (operator.equals("+") || operator.equals("-")) {
            return 1;
        } else if (operator.equals("*") || operator.equals("/")) {
            return 2;
        }
        return 0;
    }


    public String toRPN(String input) {
        String outputAsRPN = "";

        Stack<String> stack = new Stack<>();

        if (input == "") {
            return "0";
        }
        if (input.charAt(0) == '-') {
            input = "0" + input;
        }
        input = input.trim();
        StringTokenizer stringTokenizer = new StringTokenizer(input, notDigits, true);

        while (stringTokenizer.hasMoreTokens()) {
            String nextPart = stringTokenizer.nextToken();

            if (nextPart.equals("+") ||
                    nextPart.equals("-") ||
                    nextPart.equals("*") ||
                    nextPart.equals("/")) {

                while (!stack.isEmpty() && (priorityCheck(stack.peek()) >= priorityCheck(nextPart))) {
                    outputAsRPN += stack.pop() + " ";
                }
                stack.push(nextPart);

            } else if (nextPart.equals("(")) {
                stack.push(nextPart);

            } else if (nextPart.equals(")")) {
                while (!stack.peek().equals("(")) {
                    outputAsRPN += stack.pop();
                }
                stack.pop();

            } else {
                outputAsRPN += nextPart + " ";
            }
        }

        while (!stack.empty()) {
            outputAsRPN += stack.pop() + " ";
        }

        return outputAsRPN;
    }


    public String computeExpression(String outputRPN) {
        Stack<Double> stack = new Stack<>();
        String innerOutputPRN = outputRPN + "=";

        double numberLeft = 0;
        double numberRight = 0;
        double solution = 0;

        String createNumer = "";

        for (int i = 0; i < innerOutputPRN.length(); i++) {
            char ch = innerOutputPRN.charAt(i);

            if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                if (!stack.empty()) {

                    numberRight = stack.pop();
                    numberLeft = stack.pop();

                    if (ch == '+') {
                        solution = MathOperations.addition(numberLeft, numberRight);
                    } else if (ch == '-') {

                        solution = MathOperations.subtraction(numberLeft, numberRight);
                    } else if (ch == '*') {

                        solution = MathOperations.multiplication(numberLeft, numberRight);
                    } else if (ch == '/') {
                        if (numberRight == 0){
                            return "You should not divide by Zero!";
                        }
                        solution = MathOperations.division(numberLeft, numberRight);
                    }
                }
                stack.push(solution);

            } else if (((ch >= '0' && ch <= '9') || ch == '.')) {
                createNumer += ch;

            } else if (ch == ' ') {
                if (createNumer.compareTo("") != 0) {
                    double tmp = Double.parseDouble(createNumer);

                    stack.push(tmp);
                    createNumer = "";
                }

            } else if (ch == '=') {
                if (!stack.empty()) {
                    solution = stack.pop();
                    break;
                }
            }
        }
        double roundedSolution = Utils.round(solution);
        return String.valueOf(roundedSolution);
    }

}

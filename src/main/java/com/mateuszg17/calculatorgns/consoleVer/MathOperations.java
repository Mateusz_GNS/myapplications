package com.mateuszg17.calculatorgns.consoleVer;

public class MathOperations {

    public static double addition(double compA, double compB) {
        return compA + compB;
    }

    public static double subtraction(double compA, double compB) {
        return compA - compB;
    }

    public static double multiplication(double compA, double compB) {
        return compA * compB;
    }

    public static double division(double compA, double compB) {

        if (compB ==0){
            System.out.println("You cannot divide by zero!");
            return 0;
        }
        return compA/compB;
    }

}

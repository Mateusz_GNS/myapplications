package com.mateuszg17.calculatorgns.consoleVer;

public interface MyValidators<T> {

public T inputValidator(T inputLine);
public T secondInputValidator(T inputLine);

}

package com.mateuszg17.calculatorgns.consoleVer;

import java.util.Scanner;

public class StartApp implements IStartApp {
    public static void main(String[] args) {
        StartApp sa = new StartApp();
        sa.run();
    }

    @Override
    public void run() {
        final ReversePolishNotationLogic rpn = new ReversePolishNotationLogic();
        final Utils utils = new Utils();

        boolean flag = true;

        while (flag) {
            System.out.println("Hello, please type in calculation: \n");

            final Scanner scanner = new Scanner(System.in);
            final String input = scanner.nextLine();

            final String inputChecked = utils.inputValidator(input);

            if (inputChecked.equals(input)) {
                String secondInputCheck = utils.secondInputValidator(input);
                    if (!secondInputCheck.equals("")) {
                        String toRPN = rpn.toRPN(secondInputCheck);
                        String result = rpn.computeExpression(toRPN);
                        System.out.println(result);
                    } else {
                        System.out.println(secondInputCheck);
                    }
            }

            System.out.println("Another calculation? Y / N ");

            final String anotherCalculation = scanner.nextLine().toUpperCase();
            if (anotherCalculation.equals("N")) {
                flag = false;
            }
        }
    }
}

package com.mateuszg17.calculatorgns.consoleVer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class Utils implements MyValidators<String>{

    public static double round(double value) {

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(8, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public String inputValidator(String inputLine) {
        String regex2 = "[0-9(|\\-|\\s][0-9+*\\-\\.\\/()\\s]+"; // first place only digits, space, ( and "-" possible, further digits, space and /*-+ ()
        Pattern pattern = Pattern.compile(regex2);
        Matcher matcher = pattern.matcher(inputLine);
        boolean isInputOK = matcher.matches();

        if (isInputOK) {
            return inputLine;
        } else {
            System.out.println("Wrong input.. Only digits +-*/.() allowed");
        }
        return "";

    }

    @Override
    public String secondInputValidator(String inputLine) {
        String notNumbers = "()/*-+";

        final List<String> tokensList = Collections.list(new StringTokenizer(inputLine, notNumbers, true)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());

        int openingBracetCouner = 0;
        int closingBracetCounter = 0;

        long numberOfDots = tokensList.get(0).chars().filter(ch -> ch == '.').count();
        if (numberOfDots>1){
            System.out.println("There can be max one dot in expression");
            return "";
        }

        for (int i = 1; i < tokensList.size(); i++) {
            String oneBefore = tokensList.get(i - 1);
            String current = tokensList.get(i);

             numberOfDots = current.chars().filter(ch -> ch == '.').count();
            if (numberOfDots>1){
                System.out.println("There can be max one dot in expression");
                return "";
            }


            if (tokensList.get(0).length() > 10 || tokensList.get(i).length() > 10) { // max 10 sings per digit
                System.out.println("Numbers cannot be longer than 10 digits");
                return "";
            }

            if (oneBefore.matches("[/*-+.]") && current.matches("[/*-.+]")) { // cannot be more than one operator or dot in a row
                System.out.println("Cannot be more than one operator or dot in a row");
                return "";
            }

            if ((oneBefore.matches("[/*-+.]") && current.matches("[)]")) ||
                    (oneBefore.matches("[(]") && current.matches("[/*-+.]"))) {
                System.out.println("This operation makes no sense..");
                return "";
            }

            if (oneBefore.matches("[\\d+]") && current.matches("[(]") ||
                    (oneBefore.matches("[)]") && current.matches("[\\d+]"))) { // cannot be more than one operator or dot in a row
                System.out.println(("Before and after () must be operator, unless this is last ) in expression"));
                return "";
            }

            if ((oneBefore.matches("[/]") && current.matches("0+")) ||
                    (oneBefore.matches("[/]") && current.matches("0.0+"))) {
                System.out.println("Cannot divide by zero");
                return "";
            }
            if (current.charAt(0) == '.') { // omitting 0 before dot not allowed --> e.g " .5"
                System.out.println("Omitting 0 before dot not allowed --> e.g \" .5\"");
                return "";
            }

            if (oneBefore.matches("[(]")) {
                openingBracetCouner++;
            }

            if (current.matches("[)]")) {
                closingBracetCounter--;
            }

            if (openingBracetCouner + closingBracetCounter < 0) { // cannot closing bracket ")" be before opening "("
                System.out.println("Cannot closing bracket \")\" be before opening \"(\"");
                return "";
            }
        }

        if (openingBracetCouner + closingBracetCounter != 0) {
            System.out.println("Number of opening and closing brackets is not the same");
            return "";
        }

        String lastElementNumber = tokensList.get(tokensList.size() - 1);

        if (lastElementNumber.matches("[/*\\-+(]")) { //last element cannot be operator dot or ")"
            System.out.println("Last element cannot be operator dot or \"(\"");
            return "";
        }
        return inputLine;
    }


}

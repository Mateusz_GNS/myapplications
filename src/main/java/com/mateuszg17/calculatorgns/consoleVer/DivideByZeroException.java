package com.mateuszg17.calculatorgns.consoleVer;

public class DivideByZeroException extends RuntimeException {
    public DivideByZeroException (String s) {super(s);}
}

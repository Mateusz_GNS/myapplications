package com.mateuszg17.calculatorgns;

import com.mateuszg17.calculatorgns.model.InputLine;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CalculatorgnsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculatorgnsApplication.class, args);
    }

    @Bean
    public InputLine inputLine(){return new InputLine();}

}

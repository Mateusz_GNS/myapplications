package com.mateuszg17.calculatorgns.services;

import com.mateuszg17.calculatorgns.consoleVer.MyValidators;
import com.mateuszg17.calculatorgns.consoleVer.ReversePolishNotationLogic;
import com.mateuszg17.calculatorgns.model.InputLine;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CalculatorService implements MyValidators<InputLine> {

    private final ReversePolishNotationLogic rpn;

    @Override
    public InputLine inputValidator(InputLine input) {
        String expressionLine = input.getExpressionLine();
        input.setAlert("");

        String regex2 = "[0-9(|\\-|\\s][0-9+*\\-\\.\\/()\\s]+"; // first place only digits, space, ( and "-" possible, further digits, space and /*-+ ()
        Pattern pattern = Pattern.compile(regex2);
        Matcher matcher = pattern.matcher(expressionLine);
        boolean isInputOK = matcher.matches();

        if (!isInputOK) {
            input.setAlert("Only digits, +,-,*,/, and ( ) are allowed");
            input.setExpressionLine("0");
        }
        return input;
    }

    @Override
    public InputLine secondInputValidator(InputLine afterFirstCheck) { // only if first validation pass

        String inputAfterFirstCheck = afterFirstCheck.getExpressionLine();

        String notNumbers = "()/*-+";

        final List<String> tokensList = Collections.list(new StringTokenizer(inputAfterFirstCheck, notNumbers, true)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());

        int openingBracetCouner = 0;
        int closingBracetCounter = 0;

        long numberOfDots = tokensList.get(0).chars().filter(ch -> ch == '.').count();
        if (numberOfDots>1){
            afterFirstCheck.setAlert("There can be max one dot in expression");
            afterFirstCheck.setExpressionLine("0");
        }


        for (int i = 1; i < tokensList.size(); i++) {
            String oneBefore = tokensList.get(i - 1);
            String current = tokensList.get(i);

            numberOfDots = current.chars().filter(ch -> ch == '.').count();
            if (numberOfDots>1){
                afterFirstCheck.setAlert("There can be max one dot in expression");
                afterFirstCheck.setExpressionLine("0");
                break;
            }


            if (tokensList.get(0).length() > 10 || tokensList.get(i).length() > 10) { // max 10 sings per digit
                afterFirstCheck.setAlert("Numbers cannot be longer than 10 digits");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if (oneBefore.matches("[/*-+.]") && current.matches("[/*-.+]")) { // cannot be more than one operator or dot in a row
                afterFirstCheck.setAlert("Cannot be more than one operator or dot in a row");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if (oneBefore.matches("[\\d+]") && current.matches("[(]") ||
                    (oneBefore.matches("[)]") && current.matches("[\\d+]"))) { // cannot be more than one operator or dot in a row
                afterFirstCheck.setAlert("Before and after () must be operator, unless this is last ) in expression");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if ((oneBefore.matches("[/*-+.]") && current.matches("[)]")) ||
                    (oneBefore.matches("[(]") && current.matches("[/*-+.]"))) {
                afterFirstCheck.setAlert("This operation makes no sense..");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if ((oneBefore.matches("[/]") && current.matches("0+")) ||
                    (oneBefore.matches("[/]") && current.matches("0.0+"))) {
                afterFirstCheck.setAlert("Cannot divide by zero");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if (current.charAt(0) == '.') { // omitting 0 before dot not allowed --> e.g " .5"
                afterFirstCheck.setAlert("Omitting 0 before dot not allowed --> e.g \\\" .5\\\"");
                afterFirstCheck.setExpressionLine("0");
                break;
            }

            if (oneBefore.matches("[(]")) {
                openingBracetCouner++;
            }

            if (current.matches("[)]")) {
                closingBracetCounter--;
            }

            if (openingBracetCouner + closingBracetCounter < 0) { // cannot closing bracket ")" be before opening "("
                afterFirstCheck.setAlert("Cannot closing bracket \")\" be before opening \"(\"");
                afterFirstCheck.setExpressionLine("0");
                break;
            }
        }

        if (openingBracetCouner + closingBracetCounter != 0) {
            afterFirstCheck.setAlert("Number of opening and closing brackets is not the same");
            afterFirstCheck.setExpressionLine("0");
        }

        String lastElementNumber = tokensList.get(tokensList.size() - 1);

        if (lastElementNumber.matches("[/*-+.(]")) { //last element cannot be operator dot or ")"
            afterFirstCheck.setAlert("Last element cannot be operator, dot or \"(\"");
            afterFirstCheck.setExpressionLine("0");
        }

        return afterFirstCheck;
    }

    public InputLine validateAndCalculate(InputLine inputLine) {

        InputLine afterFirstCheck = inputValidator(inputLine);

        if (!afterFirstCheck.getAlert().equals("")) { // if there is validation alert
            return inputLine;
        } else { // first validation is OK
            InputLine afterSecondCheck = secondInputValidator(inputLine);

            if (afterSecondCheck.getAlert().equals("")) { // if second validation is OK we do calculation

                String toRPN = rpn.toRPN(inputLine.getExpressionLine());
                String result = rpn.computeExpression(toRPN);

                inputLine.setExpressionLine(result);
                return inputLine;
            } else {
                return afterSecondCheck;
            }
        }
    }

}

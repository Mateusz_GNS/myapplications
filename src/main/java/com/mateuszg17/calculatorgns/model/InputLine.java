package com.mateuszg17.calculatorgns.model;

import lombok.Data;

@Data
public class InputLine {
    private String expressionLine;
    private String alert;
}

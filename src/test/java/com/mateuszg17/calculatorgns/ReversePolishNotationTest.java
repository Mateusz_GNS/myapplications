package com.mateuszg17.calculatorgns;


import com.mateuszg17.calculatorgns.consoleVer.ReversePolishNotationLogic;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class ReversePolishNotationTest {

    ReversePolishNotationLogic rpn = new ReversePolishNotationLogic();

    @Test
    public void toRPN_1() {
        String input = "2+5";
        String expected = "2 5 + ";
        String actual = rpn.toRPN(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void computeExpression1() {
        String input = "2 5 + ";
        double expected = 7;
        double actual = Double.valueOf(rpn.computeExpression(input));

        Assert.assertEquals(expected, actual, 0.00000000001);
    }


    @Test
    public void toRPN_2() {
        String input = "-1+2*((10-8)*2)+10/10";
        String expected = "0 1 - 2 10 8 -2 ** + 10 10 / + ";
        String actual = rpn.toRPN(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void computeExpression2() {
        String input = "0 1 - 2 10 8 -2 ** + 10 10 / + ";
        double expected = 8;
        double actual = Double.valueOf(rpn.computeExpression(input));

        Assert.assertEquals(expected, actual, 0.00000000001);
    }


    @Test
    public void toRPN_3() {
        String input = "-10+(2*3+(1+1)*3)+0.5";
        String expected = "0 10 - 2 3 * 1 1 +3 *++ 0.5 + ";
        String actual = rpn.toRPN(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void computeExpression3() {
        String input = "0 10 - 2 3 * 1 1 +3 *++ 0.5 + ";
        double expected = 2.5;
        double actual = Double.valueOf(rpn.computeExpression(input));

        Assert.assertEquals(expected, actual, 0.0000000001);
    }


}

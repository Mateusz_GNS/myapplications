package com.mateuszg17.calculatorgns;

import com.mateuszg17.calculatorgns.consoleVer.DivideByZeroException;
import com.mateuszg17.calculatorgns.consoleVer.MathOperations;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class MathOperationsTests {

    @Test
    public void addition_10plus5_is15() {
        double expected = 15;

        double actual = MathOperations.addition(10, 5);

        Assert.assertEquals(expected, actual, 0.00000000001);
    }

    @Test
    public void subtraction_2minus5_isMinus3() {
        double expected = -3;

        double actual = MathOperations.subtraction(2, 5);

        Assert.assertEquals(expected, actual, 0.00000000001);
    }

    @Test
    public void multiplication_5timesMinus2_isMinus10() {
        double expected = -10;

        double actual = MathOperations.multiplication(5, -2);

        Assert.assertEquals(expected, actual, 0.00000000001);
    }

    @Test
    public void division_12dividedBy4_is3() {
        double expected = 3;

        double actual = MathOperations.division(12, 4);

        Assert.assertEquals(expected, actual, 0.00000000001);

    }

}

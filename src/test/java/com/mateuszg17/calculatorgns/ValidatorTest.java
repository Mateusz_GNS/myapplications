package com.mateuszg17.calculatorgns;

import com.mateuszg17.calculatorgns.consoleVer.Utils;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class ValidatorTest {

    Utils utils = new Utils();

    @Test
    public void inputValidator_positive() {

        String input = "156+*+++.789    7+-*/* ()  4684";
        String expected = input;
        final String actual = utils.inputValidator(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void inputValidator_returns_empty_string_when_validation_fails() {
        String input = "123 a +-*/";
        String expected = "";
        final String actual = utils.inputValidator(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void inputValidator_fails_wrong_character() {
        String input = "123 #@$ +-*/";
        String expected = "";
        final String actual = utils.inputValidator(input);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void inputValidator_Ok() {
        String input = "1/1000000";
        String expected = "1/1000000";
        final String actual = utils.inputValidator(input);

        assertTrue(expected.equals(actual));
    }


    @Test
    public void secondCheck_OK_input() {
        String input = "123+(123-456*(987/789))";
        String expected = input;
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_too_long_number() {
        String input = "12345678901+0+2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }


    @Test
    public void secondCheck_two_operators_in_a_row() {
        String input = "1++2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_nonsense_Operation(){
        String input = "1+(+2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_divide_by_zero(){
        String input = "1/0";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }


    @Test
    public void secondCheck_ommiting_zero_in_fraction() {
        String input = "1+.9+2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_closing_bracket_before_opening() {
        String input = "(1+3)+)+(+2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_opning_closing_brackets_diff(){
        String input = "1+(2*(3)";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_last_element_forbiden(){
        String input = "1+2(";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_last_element_many_dots(){
        String input = "1.2.5+2";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }

    @Test
    public void secondCheck_no_operator_btwn_number_bracket(){
        String input = "1(2+2)";
        String expected = "";
        final String actual = utils.secondInputValidator(input);

        assertEquals(expected, actual);
    }


}
